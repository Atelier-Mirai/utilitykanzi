//
//  utilitykanziAppDelegate.h
//  utilitykanzi
//
//  Created by 臼井 常雄 on 11/11/17.
//  Copyright 2011 岐阜IT協同組合. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MainViewController;

@interface utilitykanziAppDelegate : NSObject <UIApplicationDelegate> {

}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (nonatomic, retain) IBOutlet MainViewController *mainViewController;

@end
