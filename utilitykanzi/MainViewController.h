//
//  MainViewController.h
//  utilitykanzi
//
//  Created by 臼井 常雄 on 11/11/17.
//  Copyright 2011 岐阜IT協同組合. All rights reserved.
//

#import "FlipsideViewController.h"

@interface MainViewController : UIViewController <FlipsideViewControllerDelegate> {

}


- (IBAction)showInfo:(id)sender;

@end
