//
//  main.m
//  utilitykanzi
//
//  Created by 臼井 常雄 on 11/11/17.
//  Copyright 2011 岐阜IT協同組合. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[])
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    int retVal = UIApplicationMain(argc, argv, nil, nil);
    [pool release];
    return retVal;
}
